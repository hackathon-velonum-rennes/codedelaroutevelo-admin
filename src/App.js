import React from 'react';
import { Admin, Resource } from 'react-admin';
import { RestProvider, base64Uploader } from './lib';

import {
  FirebaseAuthProvider
} from "react-admin-firebase";

import { CategoriesList, CategoriesEdit, CategoriesCreate } from './resources/categories';
import { CoursList, CoursEdit, CoursCreate } from './resources/cours';

import { firebaseConfig } from './FIREBASE_CONFIG';

const trackedResources = [{ name: 'categories' }, { name: 'cours' }];

const authProvider = FirebaseAuthProvider(firebaseConfig, { trackedResources });
const dataProvider = base64Uploader(RestProvider(firebaseConfig, { trackedResources }));

const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    <Resource name="categories" list={CategoriesList} edit={CategoriesEdit} create={CategoriesCreate} />
    <Resource name="cours" list={CoursList} edit={CoursEdit} create={CoursCreate} />
  </Admin>
);
export default App;
