import React from 'react';
import {
  List, Edit, Create, Datagrid, TextField, EditButton,
  SimpleForm, TextInput, RichTextField, DeleteButton, ImageField
} from 'react-admin';
// import RichTextInput from 'ra-input-rich-text';
// => compile error !

export const CategoriesList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="name" />
      <RichTextField source="description" />
      <ImageField source="picture" />
      <EditButton />
      <DeleteButton label="" redirect={false} />
    </Datagrid>
  </List>
);

// <RichTextInput source="description" />
export const CategoriesEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="description" multiline />
    </SimpleForm>
  </Edit>
);

// <RichTextInput source="description" />
export const CategoriesCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="description" multiline />
    </SimpleForm>
  </Create>
);
