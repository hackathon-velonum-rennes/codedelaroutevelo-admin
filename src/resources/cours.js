import React from 'react';
import {
  List, Edit, Create, Datagrid, TextField, EditButton,
  SimpleForm, TextInput, DeleteButton, ReferenceField, ImageField
} from 'react-admin';

export const CoursList = props => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="Categorie" source="categorie_id" reference="categories">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="titre" />
      <TextField source="subtitle" />
      <TextField source="contenu" />
      <ImageField source="picture" />
      <EditButton />
      <DeleteButton label="" redirect={false} />
    </Datagrid>
  </List>
);

export const CoursEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="titre" />
    </SimpleForm>
  </Edit>
);

export const CoursCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="titre" />
    </SimpleForm>
  </Create>
);
